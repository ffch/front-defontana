export class DatosClima {
    high:       number;
    low:        number;
    dryDays:    number;
    snowDays:   number;
    rainfall:   number;
}