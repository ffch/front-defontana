import { DatosClima } from './datosClima.model';

export class Lugar {
    id:         number;
    city:       string;
    country:    string;
    monthlyAvg: [DatosClima];
}
