import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LugaresGridComponent } from './components/lugares-grid/lugares-grid.component';



const routes: Routes = [
  { path: '',   redirectTo: 'lugares', pathMatch: 'full' },
  { path: 'lugares', component: LugaresGridComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
