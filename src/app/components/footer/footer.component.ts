import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  public fechaActual = new Date;
  public soloAnio: number = 0;

  constructor() { }

  ngOnInit(): void {

    this.soloAnio = this.fechaActual.getFullYear()
  }

}
