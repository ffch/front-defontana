import { Component, Input, OnInit } from '@angular/core';
import { Lugar } from 'src/app/models/lugar.model';

@Component({
  selector: 'app-lugar-card',
  templateUrl: './lugar-card.component.html',
  styleUrls: ['./lugar-card.component.css']
})
export class LugarCardComponent implements OnInit {

  @Input() lugar: Lugar;
  public promedioTemperaturaMaxima: number = 0;
  public promedioTemperaturaMinima: number = 0;
  public totalAnualDiasSecos:       number = 0;
  public totalAnualDiasConLluvia:   number = 0;
  public totalAnualDiasConNieve:    number = 0;
  public imagenUrl:                string = '';
  public imagenUrl2:                string = '';
  public banderaUrl:               string = '';
  public descripcionCiudad:         string ='';

  constructor() { }

  ngOnInit(): void {
    
    this.configurarUrlImagenes();

    this.calcularTotalesYPromedios();
    
  }

  configurarUrlImagenes(): void{
    if( this.lugar.city === 'Amsterdam'){
      this.imagenUrl = 'assets/img/amsterdam.jpg';
      this.imagenUrl2 = 'assets/img/amsterdam2.jpg';
      this.banderaUrl = 'assets/img/banderas/i-holanda.png';
      this.descripcionCiudad = 'Amsterdam significa el “dique del Amstel”, ' + 
        'es la capital constitucional de Holanda y desde el siglo XVII un ' + 
        'importante centro financiero y cultural. La Universidad de Amsterdam ' + 
        'fue abierta en el año 1632. La producción agropecuaria, los viveros con ' + 
        'los famosos tulipanes y los molinos que hoy rodean la ciudad, son las postales ' + 
        'típicas del país. Más la economía de Amsterdam y de toda Holanda se halla ' + 
        'consolidada en empresas de energía, servicios financieros, tecnología y ' + 
        'actividades comerciales diversas.';

    } else if( this.lugar.city === 'Athens'){
      this.imagenUrl = 'assets/img/atenas.jpg';
      this.imagenUrl2 = 'assets/img/atenas2.jpg';
      this.banderaUrl = 'assets/img/banderas/i-greece.png';
      this.descripcionCiudad = 'Atenas (en Griego Αθήνα, Athína) es la Capital de Grecia ' + 
        'y actualmente la ciudad más grande del país. La población del municipio de Atenas ' + 
        'es de 741.512 (en 2001), pero su Área metropolitana es mucho mayor y comprende una ' + 
        'población de 3,7 millones (en 2005). Es el centro principal de la vida económica, ' + 
        'cultural y política griega. La historia de Atenas se extiende más de 3000 años, lo ' + 
        'que la convierte en una de las ciudades habitadas más antiguas. Durante la época ' + 
        'clásica de Grecia, fue una poderosa Ciudad estado que tuvo un papel fundamental en ' + 
        'el desarrollo de la Democracia.';

    }else if( this.lugar.city === 'Atlanta GA'){
      this.imagenUrl = 'assets/img/atlanta.jpg';
      this.imagenUrl2 = 'assets/img/atlanta2.jpg';
      this.banderaUrl = 'assets/img/banderas/i-eeuu.png';
      this.descripcionCiudad = 'Atlanta . Es una de las ciudades más grandes e importantes ' + 
        'y la tercera ciudad más poblada de Estados Unidos. Se le ha considerado como "la capital ' + 
        'del nuevo Sur", "la ciudad internacional" y "el mejor lugar para los negocios". Está ubicada ' + 
        'al Sur de Estados Unidos, al noroeste de Georgia y bordeada, al Oeste, por el río Chattahoochee ' + 
        'y al Este por Stone mountain, una formación rocosa que guarda también un parque.';

    }else if( this.lugar.city === 'Auckland'){
      this.imagenUrl = 'assets/img/auckland.jpg';
      this.imagenUrl2 = 'assets/img/auckland2.jpg';
      this.banderaUrl = 'assets/img/banderas/i-new-zealand.png';
      this.descripcionCiudad = 'Auckland. Antigua capital de Nueva Zelanda, situada en la Isla Norte, ' + 
        'en la zona más septentrional. La ciudad concentra la principal actividad económica y comercial ' + 
        'del país. Popularmente se la conoce como la ciudad de los veleros por la cantidad de barcos que ' + 
        'pueblan sus aguas. Posee la proporción de barco por persona más alta del mundo gracias a la calidad ' +
        'de sus vientos y a su buen tiempo.';

    }else if( this.lugar.city === 'Austin TX'){
      this.imagenUrl = 'assets/img/austin.jpg';
      this.imagenUrl2 = 'assets/img/austin2.jpg';
      this.banderaUrl = 'assets/img/banderas/i-eeuu.png';
      this.descripcionCiudad = 'Austin es la ciudad capital del estado de Texas (Estados Unidos). ' + 
        'Además de sus funciones como sede del gobierno del estado, Austin es un centro comercial, ' +
        'fabril, docente y de convenciones. Entre su producción, destacan los artículos de alta tecnología, ' +
        'como equipos eléctricos, semiconductores y equipos informáticos.';
    }
  }

  calcularTotalesYPromedios(): void{

    this.lugar.monthlyAvg.forEach( promedioMensual => {

      this.promedioTemperaturaMaxima += promedioMensual.high;
      this.promedioTemperaturaMinima += promedioMensual.low;
      this.totalAnualDiasSecos += promedioMensual.dryDays;
      this.totalAnualDiasConLluvia += promedioMensual.rainfall;
      this.totalAnualDiasConNieve += promedioMensual.snowDays;

    });

    this.promedioTemperaturaMaxima = this.promedioTemperaturaMaxima/12;
    this.promedioTemperaturaMinima = this.promedioTemperaturaMinima/12;
  }

}
