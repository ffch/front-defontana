import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LugaresGridComponent } from './lugares-grid.component';

describe('LugaresGridComponent', () => {
  let component: LugaresGridComponent;
  let fixture: ComponentFixture<LugaresGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LugaresGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LugaresGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
