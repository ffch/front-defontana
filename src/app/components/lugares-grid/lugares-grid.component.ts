import { Component, OnInit } from '@angular/core';
import { Lugar } from 'src/app/models/lugar.model';
import { LugarService } from 'src/app/services/lugar.service';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-lugares-grid',
  templateUrl: './lugares-grid.component.html',
  styleUrls: ['./lugares-grid.component.css']
})
export class LugaresGridComponent implements OnInit {

  public lugares: Lugar[] = [];

  constructor( private lugarService: LugarService,
    private utils: UtilsService) { }

  ngOnInit(): void {

    this.cargarLugares();
  }

  cargarLugares(): void{

    this.utils.showSpinner();
    this.lugarService.obtenerLugares().subscribe( data => {

      if( data.status == 200 ){

        this.lugares = data.body;
        this.utils.hideSpinner();
      }
    }, error => {
      console.log(error);
      this.utils.hideSpinner();
    });

  }

}
