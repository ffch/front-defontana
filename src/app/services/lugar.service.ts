import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { config } from 'src/environments/environment';
import { Lugar } from '../models/lugar.model';

@Injectable({
  providedIn: 'root'
})
export class LugarService {

  private httpOption = new HttpHeaders({
    'Content-Type': 'application/json'
  })

  constructor( private http:HttpClient ) { }

  obtenerLugares(): Observable<any>{

    return this.http.get<Lugar>( config.API_URL_NODE, {observe: 'response', headers: this.httpOption} );
  }
}
