import { Injectable } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor( private spinner: NgxSpinnerService ) { }

  /**
   * Método que despliega el spinner en pantalla
   */
  showSpinner(){
    this.spinner.show(undefined, {
      type: "ball-square-clockwise-spin",
      size: "medium",
      bdColor: "rgba(0, 0, 0, 0.8)",
      color: "white"
    });
  }

  /**
   * Método que oculta el spinner
   */
  hideSpinner(){
    this.spinner.hide();
  }

}
